<?php
/**
 * 获取指定日期对应星座
 *
 * @param integer $month 月份 1-12
 * @param integer $day 日期 1-31
 * @return boolean|string
 */
function getConstellation($month, $day)
{
	$xingzuo = '';
	// 检查参数有效性
	if ($month < 1 || $month > 12 || $day < 1 || $day > 31)
	{
		return $xingzuo;
	}	
	if(($month == 1 && $day >= 20) || ($month == 2 && $day <= 18)) {
		$xingzuo = "水瓶";
	}else if(($month == 2 && $day >= 19) || ($month == 3 && $day <= 20)) {
		$xingzuo = "双鱼";
	}else if (($month == 3 && $day >= 21) || ($month == 4 && $day <= 19)) {
		$xingzuo = "白羊";
	}else if (($month == 4 && $day >= 20) || ($month == 5 && $day <= 20)) {
		$xingzuo = "金牛";
	}else if (($month == 5 && $day >= 21) || ($month == 6 && $day <= 21)) {
		$xingzuo = "双子";
	}else if (($month == 6 && $day >= 22) || ($month == 7 && $day <= 22)) {
		$xingzuo = "巨蟹";
	}else if (($month == 7 && $day >= 23) || ($month == 8 && $day <= 22)) {
		$xingzuo = "狮子";
	}else if (($month == 8 && $day >= 23) || ($month == 9 && $day <= 22)) {
		$xingzuo = "处女";
	}else if (($month == 9 && $day >= 23) || ($month == 10 && $day <= 23)) {
		$xingzuo = "天秤";
	}else if (($month == 10 && $day >= 24) || ($month == 11 && $day <= 22)) {
		$xingzuo = "天蝎";
	}else if (($month == 11 && $day >= 23) || ($month == 12 && $day <= 21)) {
		$xingzuo = "射手";
	}else if (($month == 12 && $day >= 22) || ($month == 1 && $day <= 19)) {
		$xingzuo = "摩羯";
	}	
	return $xingzuo.'座';
}

/**
 * 根据指定日期判断星期几
*/
function getWeekNum($current_date){
	$weekarray=array("日","一","二","三","四","五","六");
	return "星期".$weekarray[date("w",strtotime($current_date))];
}


/**
 * 将日期转换为中文
 */
function dateToChinese($date)
{
	$chineseDate = '';
	//$date = '2018-10-29'
	if (false == empty($date)) {
		$chineseArr = array('零', '一', '二', '三', '四', '五', '六', '七', '八', '九');//把数字化为中文
		$chineseTenArr = array('', '十', '二十', '三十');//十位数对应中文
		$year = date('Y', strtotime($date));
		$month = date('m', strtotime($date));
		$day = date('d', strtotime($date));
		//转换为数组
		$yearArr = str_split($year);
		foreach ($yearArr as $value) {
			$chineseDate .= $chineseArr[$value];
		}
		$chineseDate .= '年';
		$monthArr = str_split($month);
		//月，日去除零
		if ($monthArr[1] != 0) {
			$chineseDate .= $chineseTenArr[$monthArr[0]] . $chineseArr[$monthArr[1]] . '月';
		} else {
			$chineseDate .= $chineseTenArr[$monthArr[0]] . '月';
		}
		$dayArr = str_split($day);
		if ($dayArr[1] != 0) {
			$chineseDate .= $chineseTenArr[$dayArr[0]] . $chineseArr[$dayArr[1]] . '日';
		} else {
			$chineseDate .= $chineseTenArr[$dayArr[0]] . '日';
		}
	}
	return $chineseDate;
}

//判断636f7079e799bee5baa631333337616534是否为日期格式，默认时间格式为Y-m-d
function is_date($dateStr,$fmt="Y-m-d"){
    $dateArr = explode("-",$dateStr);
    if(empty($dateArr)){
        return false;
    }
    foreach($dateArr as $val){
        if(strlen($val)<2){
            $val="0".$val;
        }
        $newArr[]=$val;
    }
    $dateStr =implode("-",$newArr);
    $unixTime=strtotime($dateStr);
    $checkDate= date($fmt,$unixTime);
    if($checkDate==$dateStr)
        return true;
    else
        return false;
}
 
//通过出生年月获取属相
function getShuXiang($year){     
	$animals = array(
			'鼠', '牛', '虎', '兔', '龙', '蛇',
			'马', '羊', '猴', '鸡', '狗', '猪'
	);
	$key = ($year - 1900) % 12;
	return $animals[$key];  
}

function getChineseStartTime($time)
{
	if($time>=0 && $time<=23 )
	{
		$Name = array( 
								"00:00"=>"深夜零点","01:00"=>"凌晨一点","02:00"=>"凌晨两点","03:00"=>"凌晨三点",
				                "04:00"=>"凌晨四点","05:00"=>"凌晨五点","06:00"=>"早晨六点","07:00"=>"早晨七点",
				                "08:00"=>"早晨八点","09:00"=>"上午九点","10:00"=>"上午十点","11:00"=>"上午十一点",
				                 "12:00"=>"中午十二点","13:00"=>"下午一点","14:00"=>"下午两点","15:00"=>"下午三点",
				                 "16:00"=>"下午四点","17:00"=>"下午五点","18:00"=>"下午六点","19:00"=>"晚上七点",
				                 "20:00"=>"晚上八点","21:00"=>"晚上九点","22:00"=>"晚上十点","23:00"=>"晚上十一点"
						);
		return $Name[$time];
	}
	return $time;
}


function getChineseEndTime($time)
{
	if($time>=0 && $time<=23 )
	{
		$Name = array(
				"00:59"=>"凌晨一点","01:59"=>"凌晨两点","02:59"=>"凌晨三点","03:59"=>"凌晨四点",
				"04:59"=>"凌晨五点","05:59"=>"早晨六点","06:59"=>"早晨七点","07:59"=>"早晨八点",
				"08:59"=>"上午九点","09:59"=>"上午十点","10:59"=>"上午十一点","11:59"=>"中午十二点",
				"12:59"=>"下午一点","13:59"=>"下午两点","14:59"=>"下午三点","15:59"=>"下午四点",
				"16:59"=>"下午五点","17:59"=>"下午六点","18:59"=>"晚上七点","19:59"=>"晚上八点",
				"20:59"=>"晚上九点","21:59"=>"晚上十点","22:59"=>"晚上十一点","23:59"=>"深夜零点"
		);
		return $Name[$time];
	}
	return $time;
}